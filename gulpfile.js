const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const gulpWebpack = require('gulp-webpack');

gulp.task('all', ['style', 'js'], () => {});

gulp.task('js', () => {
  return gulp.src('./src/frontend')
    .pipe(gulpWebpack(webpackConfig))
    .pipe(gulp.dest('static/script/'));
});

gulp.task('style', () => {
  return gulp.src('./src/frontend/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./static/style/'));
});

const webpackConfig = {
  context: __dirname + '/src/frontend',
  entry: {
    libs: './libs.js',
    bundle: './main.js'
  },
  output: {
    path: __dirname + '/static/script/',
    filename: './[name].min.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader?presets[]=es2015'
      }
    ]
  }
};
