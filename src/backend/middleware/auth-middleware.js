'use strict';

const passport = require('koa-passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const config = require('config');
const { User } = require('../models');

passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.get('authSecretKey')
}, (jwt_payload, done) => {
  User
    .findById(jwt_payload.id)
    .then(user => {
      if (user) {
        done(null, user);
      } else {
        done(new Error('User not found'));
      }
    })
    .catch(err => done(err));
}));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id).then((err, user) => done(err, user));
});

module.exports.passport = passport;

module.exports.middleware = async (ctx, next) => {
  try {
    await passport.authenticate('jwt', {session: false})(ctx, () => {});
  } catch (e) {
    ctx.throw(401);
  }

  if (!ctx.isAuthenticated()) {
    ctx.throw(401);
  }

  await next();
};