'use strict';

function log(...props) {
  /* eslint no-console: "off" */
  console.log(...props);
}

function error(...props) {
  /* eslint no-console: "off" */
  console.log(...props);
}

async function loggerMiddleware(ctx, next) {
  await next();
  log(`${ctx.res.statusCode} ${ctx.method} ${ctx.originalUrl}`);
}

module.exports = {
  log,
  error,
  loggerMiddleware
};