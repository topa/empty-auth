'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const User = mongoose.Schema({
  login: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    required: true
  }
});

User.pre('save', function(next) {
  if (!this.isModified('password')) {
    return next();
  }

  const SALT_ROUNDS = 10;

  bcrypt
    .genSalt(SALT_ROUNDS)
    .then(salt => bcrypt.hash(this.password, salt))
    .then(hash => {
      this.password = hash;
      next();
    });
});

User.methods.comparePassword = function(candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password);
};

module.exports = mongoose.model('User', User);