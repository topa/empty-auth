const mongoose = require('mongoose');
const config = require('config');
const fs = require('fs');
const path = require('path');

mongoose.Promise = Promise;
mongoose.connect(config.get('mongo').connection_string);

const db = { mongoose };
fs.readdirSync(__dirname)
  .filter(file => file !== 'index.js')
  .forEach(file => {
    const model = require(path.join(__dirname, file));
    db[model.modelName] = model;
  });

module.exports = db;