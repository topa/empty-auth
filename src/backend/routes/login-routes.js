'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');
const Router = require('koa-router');
const loginRouter = new Router();

const { User } = require('../models');

loginRouter.post('/login', async ctx => {
  const { login, password } = ctx.request.body;

  if (!login || !password) {
    ctx.status = 400;
    return;
  }

  const user = await User.findOne({ login });
  const passwordCorrect = user && (await user.comparePassword(password));

  if (!passwordCorrect) {
    ctx.status = 401;
    return;
  }

  const token = jwt.sign({id: user.id}, config.get('authSecretKey'));

  ctx.status = 200;
  ctx.body = { token, user };
});

module.exports = loginRouter;