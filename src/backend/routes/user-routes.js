'use strict';

const Router = require('koa-router');
const loginRouter = new Router();

const { User } = require('../models');
const { middleware: authMiddleware } = require('../middleware/auth-middleware');

loginRouter.get('/users/:id', async ctx => {
  const { id } = ctx.params;
  const user = await User.findOne({ _id: id });

  if (!user) {
    ctx.status = 404;
    return;
  }

  ctx.body = { user };
  ctx.status = 200;
});

loginRouter.get('/users', async ctx => {
  const users = await User.find({});
  ctx.body = { users };
  ctx.status = 200;
});

loginRouter.put('/users/:id', authMiddleware, async ctx => {
  const { id } = ctx.params;
  const { user: userData } = ctx.request.body;

  const user = await User.findByIdAndUpdate(id, {
    $set: userData
  }, { new: true });

  if (!user) {
    ctx.status = 404;
    return;
  }

  ctx.body = { user };
  ctx.status = 200;
});

loginRouter.post('/users', async ctx => {
  const { user: userData } = ctx.request.body;

  try {
    const user = new User(userData);
    await user.save();

    ctx.status = 201;
    ctx.body = user.toJSON();
  } catch (err) {
    if (err.errors) {
      ctx.body = Object
        .keys(err.errors)
        .map(eName => err.errors[eName].message)
        .join('\n');
      ctx.status = 400;
      return;
    }

    if (err.type === '11000') {
      // duplicate key error index
      ctx.body = 'instance is already exists';
      ctx.status = 400;
      return;
    }

    throw err;
  }
});

module.exports = loginRouter;