'use strict';

const fs = require('fs');
const path = require('path');

const Router = require('koa-router');
const apiRouter = new Router();

fs.readdirSync(__dirname)
  .filter(file => file !== 'index.js')
  .forEach(file => {
    const route = require(path.join(__dirname, file));
    apiRouter.use('/api/v1', route.routes());
  });

module.exports = { apiRouter };