'use strict';

const http = require('http');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');
const { passport } = require('./backend/middleware/auth-middleware');
const Pug = require('koa-pug');

const errorHandler = require('./backend/utils/error-handler-middleware');
const logger = require('./backend/utils/logger');
const { apiRouter } = require('./backend/routes');
const frontendRouter = require('./frontend/router');

function start({ port }) {
  const app = new Koa();

  app.use(errorHandler);
  app.use(logger.loggerMiddleware);
  app.use(passport.initialize());
  app.use(bodyParser());
  app.use(apiRouter.routes());
  app.use(apiRouter.allowedMethods());
  new Pug({ app, viewPath: './src/frontend' });
  app.use(frontendRouter.routes());
  app.use(serve(__dirname + '/../static'));

  const server = http.createServer(app.callback());

  server.listen(port);
  return { app, server };
}

if (!module.parent) {
  const port = process.env.PORT || 3000;
  start({ port });
  logger.log(`listening ${port}`);
}

module.exports = start;
