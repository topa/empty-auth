const Router = require('koa-router');
const router = new Router();

router.get('/', async ctx => {
  ctx.render('./components/index.pug');
});

module.exports = router;