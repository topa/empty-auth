module.exports = app => app.controller('login',
['$scope', 'api', function ($scope, api) {
  $scope.login = '';
  $scope.password = '';

  $scope.auth = () => {
    api
      .post('login', {
        login: $scope.login,
        password: $scope.password
      })
      .then(response => {
        console.log('response', response);
      });
  };
}]);