module.exports = app => app.service('api',
['$http', function ($http) {

  this._server = '/api/v1';

  this.get = (path, data) => {
    return $http.get(this._server + path, data);
  };

  this.post = request => {
    return $http.post(this._server + request);
  };

  this.put = request => {
    return $http.put(this._server + request);
  };

  this.delete = request => {
    return $http.delete(this._server + request);
  };

}]);