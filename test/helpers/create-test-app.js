'use strict';
const { agent } = require('supertest');
const start = require('../../src/server');
const models = require('../../src/backend/models');

module.exports = () => {
  const context = {};
  return Promise
    .resolve(start(0))
    .then(_ => {
      return models.mongoose.connection.db
        .dropDatabase()
        .then(() => Promise.resolve(_));
    })
    .then(({ app, server }) => {
      context.app = app;
      context.server = server;
      context.originalAgent = agent(server);
      context.models = models;

      context.headers = {};
      context.agent = ['get', 'post', 'put', 'delete'].reduce((result, method) => {
        result[method] = url => context.originalAgent[method](url).set(context.headers);
        return result;
      }, {});

      return context;
    });
};