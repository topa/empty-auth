'use strict';

import test from 'ava';
import createApp from '../helpers/create-test-app';

test.serial.beforeEach(async t => {
  t.context = await createApp({ createUser: false });
});

test.serial.afterEach.always(async t => {
  t.context.server.close();
});

test.serial('POST /users', async t => {
  const { agent } = t.context;

  const user = { login: 'name', password: 'pass' };
  const res = await agent.post('/api/v1/users').send({ user });

  t.true(res.status === 201);
});

test.serial('GET /users', async t => {
  const { agent } = t.context;

  const user = { login: 'name', password: 'pass' };
  const createRes = await agent.post('/api/v1/users').send({ user });
  t.true(createRes.status === 201);

  const getRes = await agent.get('/api/v1/users');
  t.true(getRes.status === 200);
  const list = getRes.body.users;
  t.true(list.length === 1);
  t.true(list[0].login === user.login);
});

test.serial('GET /users/:id', async t => {
  const { agent, models } = t.context;

  const user = { login: 'name', password: 'pass' };
  const createRes = await agent.post('/api/v1/users').send({ user });
  t.true(createRes.status === 201);

  const usersList = await models.User.find({});
  t.true(usersList.length === 1);

  const getRes = await agent.get(`/api/v1/users/${usersList[0]._id}`);
  t.true(getRes.status === 200);
  const userObject = getRes.body.user;
  t.truthy(userObject);
  t.true(userObject.login === user.login);
});

test.serial('PUT /api/v1/users/:id', async t => {
  const { agent } = t.context;

  const user = { login: 'name', password: 'pass' };
  const createRes = await agent.post('/api/v1/users').send({ user });
  t.true(createRes.status === 201);

  const authRes = await agent.post('/api/v1/login').send({ login: user.login, password: user.password });
  t.true(authRes.status === 200);
  const authToken = authRes.body.token;
  t.truthy(authToken);

  const usersListRes = await agent.get('/api/v1/users');
  t.true(usersListRes.status === 200);
  t.true(usersListRes.body.users.length === 1);
  const userObject0 = usersListRes.body.users[0];

  userObject0.name = 'new name';

  const putRes = await agent.put(`/api/v1/users/${userObject0._id}`)
    .set({ 'authorization': authToken })
    .send({ user: userObject0 });
  t.true(putRes.status === 200);

  const getUserRes = await agent.get(`/api/v1/users/${userObject0._id}`);
  t.true(getUserRes.status === 200);
  const userObject1 = getUserRes.body.user;
  t.true(userObject1.login === userObject0.login);
});