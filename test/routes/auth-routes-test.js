'use strict';

import test from 'ava';
import createApp from '../helpers/create-test-app';

test.serial.beforeEach(async t => {
  t.context = await createApp({ createUser: false });
});

test.serial.afterEach.always(async t => {
  t.context.server.close();
});

test.serial('auth test', async t => {
  const { agent } = t.context;

  const user = { login: 'name', password: 'pass' };
  const createUserRes = await agent.post('/api/v1/users').send({ user });
  t.true(createUserRes.status === 201);

  const failedLoginRes = await agent.post('/api/v1/login').send({
    login: user.login,
    password: user.password + 'fake'
  });
  t.true(failedLoginRes.status === 401);

  const realLoginRes = await agent.post('/api/v1/login').send({
    login: user.login,
    password: user.password
  });
  t.true(realLoginRes.status === 200);
});